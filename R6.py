import pandas as pd
import numpy as np


dfs3 = pd.read_excel(r'HR-scores.xlsx', header=1)
dfs4 = pd.read_excel(r'HR-positionData.xlsx', header=2)

dfsJJ = pd.merge(dfs3, dfs4, on='EmpID', how='left')

for x in dfsJJ.index:
    if dfsJJ.loc[x, "TermReason"] == "N/A - Has not started yet":
        dfsJJ.loc[x, "EmpSatisfaction"] = np.nan

dfsJJ.drop(dfsJJ.iloc[:, 8:17], inplace=True, axis=1)
print(dfsJJ.to_string())
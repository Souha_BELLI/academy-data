import pandas as pd
import numpy as np
from datetime import datetime
# Clean Location file : delete duplicates
dfs = pd.read_excel(r'Location.xlsx')

dfs.drop_duplicates(subset='City', keep='first', inplace=True)



dfs.to_csv('Location.csv', sep=';')


# Clean personal data :
# Name standardization : Name, Forename

dfs1 = pd.read_excel(r'HR-personalData.xlsx', header=2)

for x in dfs1.index:
    if dfs1.loc[x, "Employee_Name"].find(", ") == -1:
        if dfs1.loc[x, "Employee_Name"].find(",") == -1:
            y = dfs1.loc[x, "Employee_Name"].split(" ")
            dfs1.loc[x, "Employee_Name"] = str(y[0]+", "+y[1])
        else:
            y = dfs1.loc[x, "Employee_Name"].split(",")
            dfs1.loc[x, "Employee_Name"] = str(y[0] + ", " + y[1])

# Married ID correction
dfs2 = pd.read_excel(r'Marital Status.xlsx')

dfsJ = pd.merge(dfs1, dfs2, left_on='MaritalStatusID', right_on='MaritalStatusID', how='left')




# MaritalStatusID correction
for x in dfsJ.index:
    if dfsJ.isnull().loc[x, "MaritalStatusID"]:
        dfsJ.loc[x, "MarriedID_y"] = dfsJ.loc[x, "MarriedID_x"]
        if dfsJ.loc[x, "MarriedID_x"] == 1:
            dfsJ.loc[x, "MaritalStatusID"] = 1

for x in dfsJ.index:
    dfsJ.loc[x, "MarriedID_x"] = dfsJ.loc[x, "MarriedID_y"]

del dfsJ['MarriedID_y']
del dfsJ['MaritalDesc']
dfsJ.loc[x, "MaritalStatusID"] = dfsJ.loc[x, "MaritalStatusID"].astype('int')

dfsJ.rename(columns={'MarriedID_x': 'MarriedID'}, inplace=True)

# DOB correction

for x in dfsJ.index:
    if len(str(dfsJ.loc[x, "DOB"])) < 19:
        dfsJ.loc[x, "DOB"] = str(dfsJ.loc[x, "DOB"]).lstrip()
        dfsJ.loc[x, "DOB"] = datetime.strptime(dfsJ.loc[x, "DOB"], '%d/%m/%Y').date()
        dfsJ.loc[x, "DOB"] = dfsJ.loc[x, "DOB"].strftime("%d/%m/%Y")
    else:
        dfsJ.loc[x, "DOB"]=dfsJ.loc[x, "DOB"].strftime("%d/%m/%Y")


print(dfsJ.to_string())




import pandas as pd
import numpy as np
## imporation des fichier source
df1_Personal = pd.read_excel("HR-personalData.xlsx", header =2)
## Ajout de la colone city à partir de l'adress
for i in df1_Personal.index :
    if  df1_Personal.loc[i, "Address"].find("82 AVENUE DENFERT-ROCHEREAU -") != -1 :
        df1_Personal.loc[i, "City"] = df1_Personal.loc[i, "Address"].split(r"82 AVENUE DENFERT-ROCHEREAU -")[1]
    elif df1_Personal.loc[i, "Address"].find("-REGNAULT -") != -1 :
        df1_Personal.loc[i, "City"] = df1_Personal.loc[i, "Address"].split(r"-REGNAULT -")[1]
    elif  df1_Personal.loc[i, "Address"].find("-") != -1 :
            df1_Personal.loc[i, "City"] = df1_Personal.loc[i, "Address"].split(r"-")[1]
    elif   df1_Personal.loc[i, "Address"].find("- ") != -1 :
        df1_Personal.loc[i, "City"] = df1.loc[i, "Address"].split(r"- ")[1]
    elif  df1_Personal.loc[i, "Address"].find("VAUGIRARD") != -1 :
        df1_Personal.loc[i, "City"] = df1_Personal.loc[i, "Address"].split(r"VAUGIRARD")[1]
    elif df1_Personal.loc[i, "Address"].find("25 RUE MANIN - 75940") != -1 :
        df1_Personal.loc[i, "City"] = df1_Personal.loc[i, "Address"].split(r"5 RUE MANIN - 75940")[1]
## supression de l'espace dans la colone city
df1_Personal.City = df1_Personal.City.str.replace('^ ', '')
print(df1_Personal.to_string())
import pandas as pd
import numpy as np
## import des fichier source
df1 = pd.read_excel("HR-personalData.xlsx",header=2)
df4 = pd.read_excel('Location.xlsx')
## remplacer le nom de la colonne city dans le fichier location
df4.rename(columns={'City': 'City1'}, inplace=True)
## Ajout de la colonne City dans le fichier HRpersonaData
for i in df1.index :
    if  df1.loc[i, "Address"].find("82 AVENUE DENFERT-ROCHEREAU -") != -1 :
        df1.loc[i, "City"] = df1.loc[i, "Address"].split(r"82 AVENUE DENFERT-ROCHEREAU -")[1]
    elif df1.loc[i, "Address"].find("-REGNAULT -") != -1 :
        df1.loc[i, "City"] = df1.loc[i, "Address"].split(r"-REGNAULT -")[1]
    elif  df1.loc[i, "Address"].find("-") != -1 :
            df1.loc[i, "City"] = df1.loc[i, "Address"].split(r"-")[1]
    elif   df1.loc[i, "Address"].find("- ") != -1 :
        df1.loc[i, "City"] = df1.loc[i, "Address"].split(r"- ")[1]
    elif  df1.loc[i, "Address"].find("VAUGIRARD") != -1 :
        df1.loc[i, "City"] = df1.loc[i, "Address"].split(r"VAUGIRARD")[1]
    elif df1.loc[i, "Address"].find("25 RUE MANIN - 75940") != -1 :
        df1.loc[i, "City"] = df1.loc[i, "Address"].split(r"5 RUE MANIN - 75940")[1]
 ##supression de l'espace dans la colonne city
df1.City = df1.City.str.replace("^ ","")
## Jointure du Dataframe HrPersonal avec le location
df_jointure = pd.merge(df1, df4, left_on='City',right_on='City1', how='left')
##Rechcercher la résence des city  du fichier location dans le fichier HRpersonal
for x in df_jointure.index:
    if df_jointure.loc[x, 'City'] != df_jointure.loc[x, 'City1']:
        df_jointure.loc[x, 'City'] = "Autres"
del df_jointure['City1']
print(df_jointure.to_string())



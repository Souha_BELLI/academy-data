import pandas as pd
import numpy as np

#Importation des tables
df = pd.read_excel("HR-personalData.xlsx", header=2)
df1 = pd.read_excel("HR-positionData.xlsx", header=2)
df2 = pd.read_excel("HR-scores.xlsx", header=1)

#ajout du numéro 9 HR-personalData
for x in df.index:
    if len(str(df.loc[x, 'EmpID'])) == 9:
        df.loc[x, 'EmpID'] = ('9'+str(df.loc[x, 'EmpID']))

#ajout du numéro 9 HR-positionData.xlsx

for x in df1.index:
    if len(str(df1.loc[x, 'EmpID'])) == 9:
        df1.loc[x, 'EmpID'] = ('9'+str(df1.loc[x, 'EmpID']))

#ajout du numéro 9 HR-scores.xlsx
for x in df2.index:
    if len(str(df2.loc[x, 'EmpID'])) == 9:
        df2.loc[x, 'EmpID'] = ('9'+str(df2.loc[x, 'EmpID']))
#unique(EmpID)
df.drop_duplicates(subset=['EmpID'], inplace=True, ignore_index=True)
df1.drop_duplicates(subset=['EmpID'], inplace=True, ignore_index=True)
df2.drop_duplicates(subset=['EmpID'], inplace=True, ignore_index=True)

#Afiichage
print(df.to_string())
print(df1.to_string())
print(df2.to_string())

#to_excel
df.to_excel('HR-personalData_nettoyé.xlsx')
df1.to_excel('HR-positionData_nettoyé.xlsx')
df2.to_excel('HR-scores_nettoyé.xlsx')
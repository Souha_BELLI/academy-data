import pandas as pd
import numpy as np

# Importation HR-positionData.xlsx
df1 = pd.read_excel("HR-positionData.xlsx", header=2)
print(df1.to_string())

# Importation 'RecruitmentSource-Modification.txt'
df2 = pd.read_csv('RecruitmentSource-Modification.txt', delimiter="\t",header=9)
df2["new"] = df2["Replace :"].str.split(r" -> ", expand=True)[1]
df2["old"] = df2["Replace :"].str.split(" -> ", expand=True)[0]

# Jointure
df = pd.merge(df1, df2, left_on='RecruitmentSource',right_on='old', how='left')

# Recherche et remplacement
for x in df.index:
    if df.loc[x, 'RecruitmentSource'] == df.loc[x, 'old']:
        df.loc[x, 'RecruitmentSource'] = df.loc[x, 'new']
# Nettoyage
del df["Replace :"],
del df["new"],
del df["old"],

# Affichage
print(df.to_string())

#Exportation vers xlsx
df.to_excel('HR-positionData_RecruitmentSource.xlsx')
